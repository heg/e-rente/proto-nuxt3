export const state = () => ({
  registry: null,
  loginRegistry: null,
  policies: null,
  selection: null,
});

export const mutations = {
  setRegistry(state, data) {
    state.registry = data;
  },
  setLoginRegistry(state, data) {
    state.loginRegistry = data;
  },
  setPolicies(state, data) {
    state.policies = data;
  },
  setSelection(state, data) {
    state.selection = data;
  },
};
