// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  runtimeConfig: {
    public: {
      dibsApiBasePath: process.env.API_BASE_PATH,
      ver: process.env.npm_package_version
        ? process.env.npm_package_version
        : null,
    },
  },

  ssr: false,
  devtools: { enabled: true },
  modules: ["nuxt3-vuex-module", "@nuxtjs/i18n"],

  i18n: {
    legacy: true,
    langDir: "locales/",
    locales: [
      { code: "fr", label: "FR", language: "fr-CH", file: "fr.js" },
      { code: "de", label: "DE", language: "de-CH", file: "de.js", dir: "ltr" },
    ],
    defaultLocale: "fr",
    fallbackLocale: "fr",
    compilation: { jit: true, strictMessage: false, escapeHtml: false },
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    "@/node_modules/vue-loading-overlay/dist/css/index.css",
    "@typeform/embed/build/css/sidetab.css",
    "~/assets/css/main.css",
    "~/assets/css/components.css",
    "~/assets/css/form.css",
    "~/assets/css/synthesis.css",
    "~/assets/css/export.css",
    "vue-select/dist/vue-select.css",
    "~/assets/css/responsive.css",
  ],

  compatibilityDate: "2024-11-05",
});
