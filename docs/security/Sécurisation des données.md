# Sécurisation des données

Ce document présente l'approche de sécurisation des données du projet DIBS.

## Types de données

Il existe deux types de données à sécuriser :

1. **Données d'authentification**  
   Ces données sont liées au service OAuth2 sur lequel repose la sécurisation des données du projet. Nous utilisons actuellement `Auth0`, mais nous envisageons de passer à `SwissID` à terme. Ces deux solutions sont compatibles et interchangeables, car elles partagent le même paradigme de connexion OAuth2.
2. **Données sensibles**  
   Nous réduisons au minimum les données sensibles stockées, bien que certaines soient nécessaires, comme le numéro AVS et les informations de connexion pour les appels aux autres API.

## Données d'authentification

Le projet DIBS est divisé en deux parties : l'interface utilisateur (en JavaScript) et l'API (en PHP). L'utilisateur interagit avec l'API via des requêtes HTTPS envoyées depuis l'interface.

Les données d'authentification sont stockées dans la session PHP de l'utilisateur. Pour cela, l'API définit plusieurs en-têtes dans les réponses des requêtes :

    Access-Control-Allow-Origin: DOMAINE DE L'INTERFACE
    Access-Control-Allow-Credentials: 'true'
    Access-Control-Allow-Headers: 'X-Requested-With, Content-Type, Accept, Origin, Authorization'
    Access-Control-Allow-Methods: 'GET', 'POST'

Cela permet de garantir que les requêtes reçues proviennent bien de l'interface utilisateur et que les sessions PHP soient utilisables.

De plus, aucun élément envoyé ne permet d'identifier ou d'usurper l'identité de l'utilisateur : le seul moyen d'accès serait de compromettre sa session PHP, ce qui complique toute tentative d'interception de données.

## Données sensibles

Les données sensibles peuvent être stockées n'importe où. Actuellement, nous utilisons une base Redis, mais nous pourrions adapter le système de stockage à tout moment vers un système de fichiers, une base MySQL, ou autre.

Chaque utilisateur possède une entrée dans la base de données contenant l'ensemble de ses données.

Pour sécuriser les données, nous avons mis en place trois niveaux de sécurité :

- Sécurisation temporelle
- Sécurisation des clés d'accès aux données
- Sécurisation des données elles-mêmes

### Sécurisation temporelle

Les données sont stockées temporairement, avec un temps de vie configurable via une variable d'environnement (TTL).

À chaque consultation des données par l'utilisateur, le compteur de durée de vie est réinitialisé.

Après expiration de ce TTL, les données sont complètement effacées, et l'utilisateur devra à nouveau fournir toutes les informations nécessaires au projet DIBS.

### Sécurisation des clés d'accès aux données

Imaginons qu'un attaquant parvienne à accéder au serveur et tente de récupérer les données d'un utilisateur. Pour y parvenir, il devra trouver la clé d'accès à ces données. Cette clé est un hash créé à partir de plusieurs informations :

Voici l'algorithme de génération du hash (en PHP) :

    $key = hash(
      HASH_ALGO,
      $oauthID . ':' . SECRET_KEY . ':' . $publicKey
    );

- **HASH_ALGO** : Algorithme de hashage défini dans une variable d'environnement du serveur ([voir la liste](https://www.php.net/manual/fr/function.hash-algos.php))
- **$oauthID** : Identification de l'utilisateur dans le système d'authentification (la variable `sub` du profil, voir [OAuth2 documentation](https://www.oauth.com/oauth2-servers/signing-in-with-google/verifying-the-user-info/))
- **SECRET_KEY** : Clé secrète stockée sur le serveur
- **publicKey** : Email de l'utilisateur

Pour deviner la clé, l'attaquant devra :

- Accéder aux données personnelles de l'utilisateur (publicKey)
- Accéder à ses informations de connexion (oauthID)
- Accéder aux variables stockées sur le serveur (HASH_ALGO, SECRET_KEY)

### Sécurisation des données elles-mêmes

Si un attaquant accède aux données stockées et parvient à lister toutes les clés, les données sont néanmoins chiffrées. Nous utilisons un chiffrement SSL avec les fonctions de base de PHP.

Voici l'algorithme de chiffrement des données :

    $password = $publicKey . ':' . SECRET_KEY;
    $tag      = "";
    $iv       = openssl_random_pseudo_bytes(
      openssl_cipher_iv_length("aes-256-gcm")
    );
    $keysalt  = openssl_random_pseudo_bytes(16);

    $key      = hash_pbkdf2(HASH_ALGO, $password, $keysalt, 20000, 32, true);
    $encryptedstring = openssl_encrypt(
      $plaintext, "aes-256-gcm", $key, OPENSSL_RAW_DATA, $iv, $tag, "", 16
    );

    return $keysalt . $iv . $encryptedstring . $tag;

L'algorithme de déchiffrement est disponible sur demande.

Pour déchiffrer les données, un attaquant devrait :

- Accéder aux données personnelles de l'utilisateur (publicKey)
- Accéder aux informations stockées sur le serveur (HASH_ALGO, SECRET_KEY)

Même s'il accédait au serveur et aux données, l'attaquant ne pourrait pas deviner l'email de l'utilisateur et devrait alors tenter une attaque par force brute sans disposer d'aucune donnée.

## Remarque générale

Pour une sécurité optimale, les échanges HTTPS entre l'interface et l'API ne doivent jamais transmettre de données sensibles. Comme toutes les données sont stockées sur le serveur et non dans le navigateur de l'utilisateur, il n'y a aucune raison que les données de connexion soient transmises entre l'API et l'interface.
