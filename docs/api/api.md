# API

Ce document regroupe, page par page, les différents appels d'API.

## / (index)

Page d'accueil.

- `GET /me` : Vérifie si l'utilisateur est connecté et récupère ses informations de connexion pour afficher, si nécessaire, la barre de connexion.

## /index2

## /prelogin

Ces deux pages fournissent des informations à l'utilisateur non connecté au sujet du processus de connexion et des conditions d'utilisation.

- `GET /me` : Vérifie si l'utilisateur est connecté. Si non, on reste sur la page ; si oui, on redirige l'utilisateur vers la page [/router](#router).

## /router

Cette page, invisible à l'utilisateur, permet de le rediriger automatiquement vers la page la plus utile.

- `GET /me` : Vérifie si l'utilisateur est connecté. Si non, on le redirige automatiquement vers [/ (index)](#index), sinon on continue les vérifications.
- `GET /openak/pension-calculator` : Récupère les projections de pension du 1er pilier pour l'utilisateur dont les informations sont enregistrées.
- - Si aucune information n'est enregistrée, redirige vers [/avs](#avs).
- - Sinon, redirige directement vers la page de synthèse [/synthesis](#synthesis).

## /avs

Page pour récupérer la projection des rentes du 1er pilier.

- `GET /me` : Vérifie si l'utilisateur est connecté. Si non, on le redirige automatiquement vers [/ (index)](#index), sinon on continue les vérifications.
- `GET /openak/insureds/[NAVS]` : Récupère certaines informations personnelles correspondantes au numéro AVS entré (via le projet MOZAR), utilisées pour pré-remplir le formulaire de la page.
- `GET /openak/pension-calculator/info` : Récupère les informations du 1er pilier déjà enregistrées par l'utilisateur.
- `POST /openak/pension-calculator/info` : Enregistre les informations du 1er pilier de l'utilisateur ; une fois enregistrées, récupère la projection via l'appel suivant.
- `GET /openak/pension-calculator` : Récupère les projections de pension du 1er pilier en fonction des informations enregistrées par l'utilisateur.

## /lpp

Page pour récupérer la projection des rentes du 2ème pilier.

- `GET /me` : Vérifie si l'utilisateur est connecté. Si non, on le redirige automatiquement vers [/ (index)](#index), sinon on continue les vérifications.
- `GET /openpk/pension-funds` : Récupère le registre des caisses de pension.
- `GET /openpk/user/pension-funds` : Récupère la caisse de pension sélectionnée par l'utilisateur.
- `POST /openpk/user/pension-funds` : Enregistre la caisse de pension sélectionnée par l'utilisateur.
- `GET /openpk/user/pension-funds/policies` : Récupère les contrats des fonds de pension de l'utilisateur.

## /3a

Page pour récupérer la projection des rentes du 3ème pilier.

- `GET /me` : Vérifie si l'utilisateur est connecté. Si non, on le redirige automatiquement vers [/ (index)](#index), sinon on continue les vérifications.
- `GET /openpv/entities` : Récupère la liste des banques.
- `GET /openpv/user/entities` : Récupère la banque sélectionnée par l'utilisateur.
- `POST /openpv/user/entities` : Enregistre la banque sélectionnée par l'utilisateur.
- `GET /openpv/user/providents` : Récupère les prévoyances de l'utilisateur.

## /synthesis

Page affichant la synthèse des informations.

- `GET /me` : Vérifie si l'utilisateur est connecté. Si non, on le redirige automatiquement vers [/ (index)](#index), sinon on continue les vérifications.
- `GET /openak/pension-calculator` : Récupère les projections de pension du 1er pilier pour l'utilisateur dont les informations sont enregistrées.
- - Si aucune information n'est enregistrée, redirige vers [/avs](#avs), car la synthèse nécessite des données du 1er pilier.
- - Sinon, on reste sur la page.
- `GET /openpk/pension-funds` : Récupère le registre des caisses de pension.
- `GET /openpk/user/pension-funds/policies` : Récupère les contrats des fonds de pension de l'utilisateur.
- `GET /open3k/info` : Récupère le résumé des informations du troisième pilier extrait des prévoyances de l'utilisateur.
- `GET /synthesis/digest` : Récupère la synthèse de toutes les informations sur la retraite générée par le projet e-rentes.ch grâce aux données fournies par l'utilisateur.
