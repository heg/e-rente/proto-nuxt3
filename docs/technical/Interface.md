# Interface

L'interface a d'abord été développée en Nuxt 2, puis migrée ultérieurement vers Nuxt 3. Bien que cette migration soit fonctionnelle, certains choix ont été effectués. Notamment, pour faciliter la transition, nous avons conservé l'utilisation de l'[Options API](https://vueschool.io/articles/vuejs-tutorials/options-api-vs-composition-api/), prévue pour [ne pas être dépréciée](https://vuejs.org/guide/extras/composition-api-faq.html#will-options-api-be-deprecated).
