# Api

- **Framework PHP** : Slim Framework 4
- **Gestion des librairies** : php-di 6
- **Client OAuth** : Une implémentation de `League\OAuth2\Client\Provider\AbstractProvider`. Actuellement, nous utilisons `League\OAuth2\Client\Provider\GenericProvider`.
- **Client HTTP** : GuzzleHttp\Client
- **Base de données** : Implémentation de `Predis\ClientInterface` via `Predis\Client`

Nous avons opté pour une pile PHP simple et flexible afin de faciliter les évolutions. Grâce à l’injection de dépendances et à la souplesse offerte par [php-di](https://php-di.org/), nous pouvons facilement réorganiser et remplacer les différentes _composantes_ de l’application en modifiant [le fichier de configuration](https://framagit.org/heg/e-rente/proto-api/-/blob/develop/config/injections.php).
