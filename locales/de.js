export default {
  commons: {
    warning_title: "Technische Information",
    disconnect: "Abmeldung",
    reset: "Meine Daten löschen",
    my_synthesis: "Meine Synthese",
    sondage_button: "Eine Bemerkung ?",
    sondage: "do9vOCwf",
    sondage_synthesis: "opTaljBr",
    from_age: "ab ",
    from_date: "ab ",
    gender: {
      MALE: "Mann",
      FEMALE: "Frau",
    },
    years_old: "0 | 1 Jahr | {n} Jahre",
    avs_options: [
      {
        label: "756.0000.0000.02 - Alice",
        value: "7560000000002",
      },
      {
        label: "756.0000.0000.19 - Beatrice",
        value: "7560000000019",
      },
      {
        label: "756.0000.0000.57 - Dan",
        value: "7560000000057",
      },
    ],
    avs_conjoint_options: [
      {
        label: "756.0000.0000.26 - Ehepartner von Beatrice",
        value: "7560000000026",
      },
      {
        label: "756.0000.0000.64 - Ehepartnerin von Dan",
        value: "7560000000064",
      },
    ],
    status: {
      just_married: {
        masc: "Verheiratet",
        fem: "Verheiratet",
        neutral: "Verheiratet",
      },
      married: {
        masc: "Verheiratet",
        fem: "Verheiratet",
        neutral: "Verheiratet",
      },
      bachelor: {
        masc: "Single",
        fem: "Single",
        neutral: "Single",
      },
      widow: {
        masc: "Verwitwet",
        fem: "Verwitwet",
        neutral: "Verwitwet",
      },
      divorced: {
        masc: "Geschieden",
        fem: "Geschieden",
        neutral: "Geschieden",
      },
      remarried: {
        masc: "Geschieden und wieder verheiratet",
        fem: "Geschieden und wieder verheiratet",
        neutral: "Geschieden und wieder verheiratet",
      },
    },
    child: "Kein Kind | 1 Kind| {n} Kinder",
    where_to_find: "Wo finden Sie das?",
    date_format: "tt.mm.jjjj",
    date_at: "am",
  },
  page: {
    about: {
      title: "Über",
      subtitle: "Der Dienst e-rentes.ch",
      content: `Der Zweck des Dienstes e-rentes.ch besteht darin, den versicherten Personen die Möglichkeit zu geben, automatisch und in Echtzeit Informationen zu ihren Renten und dem voraussichtlichen Altersvorsorgekapital aus den drei Säulen einzuholen, um ihr Renteneinkommen einzuschätzenzu berechnen.
<br><br>
Bisher ist der vorliegende Prototyp auf drei Anwendungsfällen aufgebaut und ermöglicht lediglich Simulationen anhand der drei folgenden Szenarien, die auf fiktive Profile angewendet werden: <br>
<ul>
    <li>Anpassung des Einkommens (Änderung des Beschäftigungsgrades)</li>
    <li>Vorzeitige oder aufgeschobener PensionierunRuhestandg</li>
    <li>Rente und Kapital (nur für die 2. Säule)</li>
</ul><br>
Künftig wird die Plattform individuelle Simulationen basierend auf realen Situationen ermöglichen.
<br><br>
Der Dienst e-rentes.ch wurde von einem Konsortium von öffentlichen und privaten Einrichtungen entwickelt: Dem Bundesamt für Sozialversicherungen (BSV), der Zentralen Ausgleichsstelle (ZAS) und den beiden Softwareherstellern Globaz SA und Neosis Solutions SA. Das Projekt wird von der Fachhochschule für Wirtschaft Genf geleitet und gehostet und von Innosuisse mitfinanziert.`,
    },
    index: {
      organisation_info:
        "Organisationen, die derzeit an e-rentes.ch angeschlossen sind :",
      logos: {
        random: {
          file: "logos/logorandom_de.png",
        },
      },
      exemples: {
        title: `Berechnungbeispiel`,
        users: [
          {
            photo: "exemples/photo_2.png",
            name: "Alice",
            info1: "Ledig",
            info2: "Keine Kinder",
            video: "https://youtu.be/41DgGCR5D-8",
            export:
              "N4IgtgpiBcoM4FcBGMQHMD2G0BsIFoMBDBAFwAsAmAHwEYAGADmYDYAWFgThcrYHYArPUqdalHiAA06AJYA3CADsA+oqKRUAFXJE1cGXCkgAZupk4AnqvVRoIAAoAnGcdIGjimQGMA1mo12FLpE+nAAdAAOzq7u0v62INrBoQAETi5uhtIR3qQIjgnkpKQRcNAA9OU45ADMYZjYeAhwEI5eGIqkSqRh7WDlROUAggDCaIwYXgBSADKKbF0QNTJsAOpIPpyOAMq0AFYAcgBilACSNREA8ppo+ACOAOIAIgAaAEpPe80AHm9eAKoWJ4AUUYAF44Nx8F4jDhJkQ8KglPgHgAhIwICIAEyIXSxylxqEowjY+HoNXwYk0tDY0BqLGgtFoYRYNUoAC0jBAwERzKggnoDJFopkAAJoHnmXoYMBcyU4ZQKaIyCBYmCkRwICAAX2kRDkcFOimMGBgoDUBtQghY9FtdrtlCMSBkjgoT1xCVo3Ap9FoZMd0ha31QR2BAFkhjNgU6XW6PdsNTJFGhUMIwr6wl7WR59XAE85k1aBCx03bS7b047dSACjhcTIOnByDIIpoLBFbIoEDgcNJa-XG83SjAANoAXWkXmbOCxBUUo4nIFIGFICKeMjQEDgpDNIBxq9HoDrSAgOFQDy36lIJ41RDQKSxrRSYA6uJw3nIShSb26SiMcgRLUYAESgBDYMJKEYGo2FoARdSPIgTzPOxtgQMBIAfLcUgAaSIHJVzPaQAJwIDGXtbVF33IhHWgLse2rXN103bddyow8QGbTpUEQlIMyZCDhD4WFENPVAw1fUh3ynCAUiGAA1AAJfAf06KAiMA2xKBqQQKOkKiaLonBqxwCIIiYrcd1gPdcSIdjj1ElDV0cFIjgQRQsRSfAUnEtRJI-GSVK6IxOMskAeL45liUoIT1JIzTGD4TgIKgmChHtclpGMNysVONU7E0bZNEpX0QF06zVwM7sjOyORzJYqy2OgEcEKQ1BVlaUgUlOcgCmcmoiBSAAvNCUiIHw8lPPBFBSd08jAFIAAoMHIRQZKcDA9ggcaG0UABKf8NJgBg7WkAxth5Ht1U1HVF0QdCiEcCxd2I0iaiYGoak4at9JgQzAzQnlHsq+jsgwKSVUMKzuQiOELEgToZggBRkJtPSIC8OsCixbYEQep7GUYcjAxkMBuyODBHFlKyAHcZAoGBTBwFppGh2H4dIRHkZgVGQAgYxjHRtwFFmzsqrRjGHtVHHMfxkBaEJu1Su1IA",
          },
        ],
      },
      video: {
        src: "https://www.youtube.com/embed/n61WfLTW1IU?si=9DQllfCL3udT_xfp",
        title:
          "Das Drei-Säulen-Prinzip: Erklärendes Video vom Bundesamt für Sozialversicherung",
      },
      title: "e-rentes.ch",
      subtitle:
        "Ich berechne meine Rente online anhand meiner persönlichen Daten.",
      content: "Authentifizierung erforderlich und durch SwissID gesichert",
      action: "Starten",
    },
    index2: {
      video: {
        src: "https://www.youtube.com/embed/n61WfLTW1IU?si=9DQllfCL3udT_xfp",
        title:
          "Das Drei-Säulen-Prinzip: Erklärendes Video vom Bundesamt für Sozialversicherung",
      },
      title: "Vorzubereitende Dokumente",
      subtitle: "Unverzichtbare Anmeldedaten",
      action: "Ich habe alle diese Informationen",
      tutos: [
        {
          title: "Ihre AHV- oder Krankenversicherungskarte",
          html: `Sie benötigen Ihre AHV-Nummer vom Typ 756.0000.0000.00. Ihre AHV-Nummer finden Sie auf Ihrer Lohnabrechnung oder auf Ihrer Krankenversicherungskarte.`,
        },
        {
          title: "Ihr Zugang zu Ihrer Rentenkasse der 2. Säule ",
          html: "Dies sind Ihre Verbindungsdaten zur Online-Plattform Ihrer Pensionskasse: Name Ihrer Kasse, Benutzername und Passwort.",
        },
        {
          title: "Ihr Zugang zum Ihrem Konto der 3. Säule (3a)",
          html: `Sie benötigen Ihre Benutzerkennung und Ihr Passwort, die Ihnen von der Versicherung oder der Bank, bei der Ihr 3. Säule (3a) hinterlegt ist, zugewiesen wurden, um sich in deren System einzuloggen.`,
        },
      ],
    },
    prelogin: {
      video: {
        src: "https://www.youtube.com/embed/n61WfLTW1IU?si=9DQllfCL3udT_xfp",
        title:
          "Das Drei-Säulen-Prinzip: Erklärendes Video vom Bundesamt für Sozialversicherung",
      },
      title: "Ich sichere meine Verbindung über SwissID",
      subtitle: "Was ist ",
      content: `SwissID ist ein Schweizer elektronischer Standard für sichere Identität, der eine Authentifizierung ermöglicht. Dieser Standard wird von der Schweizerischen Eidgenossenschaft und der Schweizerischen Post unterstützt.`,
      action: "Einloggen",
      cgv: {
        part_1:
          "Ich akzeptiere die Nutzungsbedingungen und den Schutz personenbezogener Daten",
        part_2: "Bedingungen zur Verwendung der Plattform e-rentes.ch",
        title: "Nutzungsbedingungen und Schutz personenbezogener Daten",
        html: `<p> INHALTSVERZEICHNIS <br />
<ul>
  <li> Warnhinweise</li>
  <li>Identifizierung</li>
  <li> Personenbezogene Daten (Anwendungsfälle und fiktive Daten)</li>
  <li> Schutz des Nutzers/der Nutzerin und seiner/ihrer personenbezogenen Daten</li>
  <li> Gewährleistungs- und Haftungsbeschränkung bei der Nutzung von e-rentes.ch</li>
  <li> Datenschutz für verheiratete Personen und eingetragene Partner</li>
  <li> Änderungen der Bedingungen</li>
</ul>
<br />
<br />
<strong>Warnhinweise</strong>
<br />
<br /> e-rentes.ch ist ein Dienst, der es Ihnen ermöglicht, online eine
Schätzung Berechnung der Altersrente und des Alterskapitals vorzunehmen. <br />
<br /> Das Ergebnis wird durch vereinfachte Verfahren ermittelt und stellt keine Garantie dar. Das Vorhandensein hypothetischer Elemente in der Errechnung impliziert, dass es sich bei den angegebenen Beträgen um Richtwerte handelt. Diese Beträge haben daher keinen rechtlichen Wert und verpflichten in keiner Weise die verschiedenen Organisationen, mit denen e-rentes.ch austauscht, in keiner Weise zur der Berechnungen austauscht. Darüber hinaus ergibt sich aus der Rentenbemessung und des Alterskapitals kein Anspruch auf eine bestimmte Leistung. <br />
<br /> Wenn Sie eine Hinterbliebenen- oder Invalidenrente beziehen oder bezogen haben, ist das Ergebnis der Bemessung Ihrer Altersrente umso weniger verlässlich. Wenn Sie sich in dieser Situation befinden, wenden Sie
sich an die Ausgleichskasse.<br />
<br />
<br />
<strong>Identifizierung/Authentifizierung</strong>
<br>
<br> Um sich für den Prototyp des Dienstes zu identifizieren, müssen Benutzer oder Benutzerin kein Konto erstellen. Sie müssen lediglich eine E- Mail-Adresse und ein fiktives Passwort eingeben und dann einen der angebotenen Anwendungsfälle (Alice, Béatrice oder Dan) auswählen. Wenn der Dienst jedoch einmal in Betrieb ist, muss e-rentes.ch den Benutzer/die Benutzerin identifizieren, damit er/sie über die <a href="https://framagit.org/heg/e-rente/proto-api" target="_blank">OpenAK- API</a> auf seine/ihre persönlichen Daten der ersten Säule zugreifen kann, um seine/ihre Anmeldung mit mehr Sicherheit zu verarbeiten. Aus diesem Grund verwendet unser Prototyp einen bestehenden Dienst (Google), um zu demonstrieren, wie eine Identifizierung über das OAuth-Protokoll durchgeführt wird. Um sich bei dem Dienst zu identifizieren, wenn dieser in Betrieb ist, benötigt der Benutzer/die Benutzerin: die Verbindungsdaten seiner/ihrer AHV-Nummer zu den verschiedenen Organisationen, mit denen er/sie für die 2. und 3. Säule (nur 3a) verbunden sein kann: Pensionskasse, Bank, Versicherung. <br> <br> Informationen zur Verbindung über einen OAuth-Dienst wie SwissID, Google oder fiktives Benutzerkonto. <br> <br> Damit der Benutzer/die Benutzerin auf personenbezogene Daten der 2. oder 3. Säule zugreifen kann, aktivieren wir offene Standards <a href="https://swissfintechinnovations.ch/projects/openpk/" target="_blank"> OpenPK</a> / <a href="https://framagit.org/heg/e-rente/openpv" target="_blank">OpenPV</a>. Jeder Benutzer/jede Benutzerin authentifiziert sich (über e-rentes.ch) bei seinem/ihrem eigenen Konto / System der 2. oder 3. Säule, sodass e- rentes.ch eine Berechung oder eine Simulation beim betreffenden System anfordern kann. Letztlich möchten wir den Identifikations- /Authentifizierungsdienst <a href="https://www.eid.admin.ch/fr" target="_blank">e-ID Suisse</a> bzw. e-ID nutzen. Das Projekt e-ID umfasst die Entwicklung einer staatlich anerkannten elektronischen Identität (e-ID) und die dazu notwendigen gesetzgeberischen Arbeiten. E-ID soll im Jahr 2026 verfügbar sein.
<br>
<br>
<strong>Personenbezogene Daten (Anwendungsfälle und fiktive Daten)</strong>
<br />
<br /> Bisher können mit dem digitalen Dienst e-rentes.ch nur Berechnungen und Simulationen zur Berechnung von Altersrenten durchgeführt werden, da es sich um einen Prototyp handelt, der anhand von fiktiven Profilen und Situationen erstellt wurde. </strong>
<br />
<br /> Die Navigationsdaten und (fiktiven) personenbezogenen Daten, auf die e-rentes.ch im Rahmen der Hochrechnung von Altersrenten zurückgreift, werden 60 Minuten lang gespeichert und danach gelöscht. Es werden nur die für die Berechnung der Altersrenten und Kapitalbeträge notwendigen Daten erfasst und auf e-rentes.ch hochgeladen. <br />
<br />
<br />
<strong>Schutz des Nutzers/der Nutzerin und seiner/ihrer personenbezogenen Daten</strong>
<br />
<br /> Es ist e-rentes.ch untersagt, auf den Inhalt der vom Nutzer/der Nutzerin online eingegebenen Daten zuzugreifen, solange diese nicht freiwillig übermittelt wurden. Darüber hinaus verpflichtet sich e-rentes.ch alle notwendigen Massnahmen zu ergreifen, um die Sicherheit und Vertraulichkeit der vom Nutzer/der Nutzerin bereitgestellten Informationen zu gewährleisten, insbesondere: <br /> - e-rentes.ch wendet eine „Do Not Track“-Richtlinie (DNT) an. Wenn Sie unseren Service besuchen, zeichnen unsere Webserver KEINE Aktivitäten oder Daten in der Protokolldatei auf.
<br /> - e-rentes.ch unterliegt dem Datenschutzgesetz das vorschreibt, dass personenbezogene Daten durch geeignete organisatorische und technische Maßnahmen vor rechtswidriger Verarbeitung geschützt werden müssen. </strong>
<br />
<br />
<br />
<strong>Gewährleistungs- und Haftungsbeschränkung bei der Nutzung von e-rentes.ch</strong>
<br />
<br /> e-rentes.ch ergreift alle geeigneten und üblichen Massnahmen, um die Integrität, Verfügbarkeit und Vertraulichkeit der Daten zu gewährleisten, die es Nutzern/Nutzerinnen oder Besuchern/Besucherinnen zur Verfügung stellt oder an diese übermittelt, sowie um sich vor Malware in seinem Netzwerk zu schützen. <br />
<br /> e-rentes.ch lehnt daher jede Verantwortung für direkte oder indirekte Verletzungen der Vertraulichkeit oder Integrität von Daten jeglicher Art ab, die ausserhalb ihres Informatiknetzwerks auftreten. <br />
<br />
<br />
<strong>Datenschutz für verheiratete Personen und eingetragene Partner</strong>
<br />
<br /> Für verheiratete Personen oder Personen, die in einer eingetragenen Partnerschaft leben, wird eine teilweise gemeinsame Rentenberechnung vorgenommen. In diesen Fällen hat jede Person Zugang zu den gemeinsamen Daten für eine individuelle Rentenberechnung. <br />
<br />
<br />
<strong>Änderungen der Bedingungen</strong>
<br />
<br /> Die Plattform e-rentes.ch behält sich das Recht vor, die vorliegenden Nutzungsbedingungen nach Rücksprache mit öffentlichen und privaten Partnern zu ändern. Nutzer/Nutzerinnen werden über die Änderungen informiert und müssen die neuen Bedingungen akzeptieren, um die Plattform weiterhin nutzen zu können.</p>`,
      },
    },
    avs: {
      video: {
        src: "https://www.youtube.com/embed/n61WfLTW1IU?si=9DQllfCL3udT_xfp",
        title:
          "Das Drei-Säulen-Prinzip: Erklärendes Video vom Bundesamt für Sozialversicherung",
      },
      title:
        "Hochrechnung meiner staatlichen Altersrente <br />(AHVG –1. Säule)",
      action: "Hochrechnen meiner staatlichen Altersrente ",
      warning: {
        body: "Andere Fälle als <q>Single</q> und <q>Verheiratet</q> werden vom Prototyp noch nicht behandelt",
      },
      modal: {
        title: "Wo finden Sie Ihre AHV-Nummer?",
        content: ` Ihre AHV-Nummer finden Sie auf Ihrem AHV-IV-Versicherungsausweis oder auf Ihrer Schweizer Krankenversicherungskarte. Dabei handelt es sich um ein physisches Dokument im Kreditkartenformat, das Sie stets bei sich tragen sollten. Sie wird auch auf Ihren Gehaltsabrechnungen ausgewiesen.
            <br /><br />
            Ihre AHV-Nummer ist vom Typ
            756.0000.0000.00`,
      },
      form: {
        labels: {
          avs: "AHV-Nummer",
          avs_neutral: "AHV-Nummer",
          avs_info: "",
          birthdate: "Geburtsdatum",
          gender: "Geschlecht",
          status: "Familienstand",
          age: "Alter",
          relationship_start: "Datum der Eheschliessung",
          relationship_end: "Datum des Endes Endes der Ehe",
          partner_avs: "AHV-Nummer des Partners / der Partnerin",
          partner_death: "Sterbedatum des Partners / der Partnerin",
          partner_birth: "Geburtsdatum des Partners / der Partnerin",
          partner_age: "Alter des Partners / der Partnerin",
          partner_gender: "Geschlecht des Partners / der Partnerin",
          child: "Kind",
          child_count: "Anzahl der Kind(er)",
          child_birth: "Geburtsdatum des Kindes",
          child_death: "Sterbedatum des Kindes",
          child_is_alive: "Lebend",
        },
      },
    },
    lpp: {
      video: {
        src: "https://www.youtube.com/embed/vJrjZGLxnSY?si=GpUt_EAwdpCIEirw",
        title: "Interview 2e pilier - Français - Marc Baijot CPEG",
      },
      title:
        "Berechnung meiner betrieblichen Altersrente (BVG – 2. Säule) <br />(BVG – 2. Säule)",
      action: "Berechnung meiner betrieblichen Altersrente",
      modal: {
        title: "Ihre Pensionskasse ",
        content: `Jeden Januar erhalten Sie per Post ein Dokument namens "<strong>Vorsorgebescheinigung</strong>". Der Name Ihrer Pensionskasse ist darin angegeben. <br/>Ihr Arbeitgeber kann es Ihnen auf Anfrage ebenfalls zur Verfügung stellen`,
      },
      form: {
        label: "Wählen Sie Ihre Pensionskasse/Vorsorgeeinrichtung aus",
      },
    },
    "3a": {
      video: {
        src: "https://www.youtube.com/embed/jRpDIHEGxh0?si=IrF7gjlKfC_X7Ler",
        title:
          "Das Drei-Säulen-Prinzip: Erklärendes Video vom Bundesamt für Sozialversicherung",
      },
      title: "Berechnung meiner privaten Vorsorge <br />(3a – 3. Säule)",
      action: "Berechnung meiner privaten Vorsorge",
      modal: {
        title: "Ihre Bank",
        content: `Benutzen Sie hier Ihren Benutzernamen und Ihr Passwort, die Ihnen von der Versicherung oder der Bank zugeteilt wurden, bei der Ihre 3. Säule (3a) deponiert ist, um sich in deren System einzuloggen.`,
        solde: {
          title: "Aktueller Kontostand",
          content: `Diese Information finden Sie auf Ihrer jährlichen Steuerbescheinigung der 3. Säule, die Sie zwischen Ende Januar und Anfang Februar in Papierform erhalten.
 <br/>
e-rentes.ch ist in der Lage, zwei Situationen der 3. Säule zu verarbeiten:
<br/>
- 3a (gebundene Vorsorgekonto mit Anlage)<br/>
- 3a (gebundene Vorsorgekonto ohne Anlage)`,
        },
        amount: {
          title: "Jährliche voraussichtliche Beiträge",
          content: `Hierbei handelt es sich um die jährlichen Beiträge, die Sie in Zukunft bis zum Rentenalter auf Ihr Konto der 3.
  Säule-(3a) einzahlen möchten. <br/><br/> Wenn Sie angestellt sind,
  können Sie den maximal zulässigen Beitrag für das laufende Jahr abziehen, nämlich CHF 7'056.- (Höchstgrenze für 2023).
  Wenn Sie selbstständig sind und keinen 2. Säule-Pensionsplan haben, können Sie bis zu 20 % Ihres Einkommens abziehen,
  jedoch ohne die Obergrenze von CHF 35'280.- pro Jahr zu überschreiten (Höchstgrenze für 2023).`,
        },
        rate: {
          title: null,
          content: null,
        },
      },
      form: {
        label: "Wählen Sie Ihre Bank aus",
        solde: "Aktueller Kontostand",
        amount: "Höhe der prognostizierten jährlichen Beiträge",
        rate: "Prognostizierter Zinssatz",
        paydate: "Gewünschtes Datum für die Kapitalauszahlung",
      },
    },
    export: {
      title: "e-rentes.ch: Berechnung der Altersrente",
      warning: `e-renten.ch ist ein Dienst, der es ermöglicht, online eine Schätzung von Renten und Alterskapital vorzunehmen. Das Ergebnis wird mithilfe vereinfachter Verfahren ermittelt und stellt keine Garantie dar. Da die Berechnung hypothetische Elemente enthält, handelt es sich bei den angegebenen Beträgen um Richtwerte. Diese Beträge haben daher keinen rechtlichen Wert und verpflichten in keiner Weise die verschiedenen Organisationen, mit denen sich e-rentes.ch zur Erstellung der Berechnungen austauscht. Darüber hinaus ergibt sich aus der Rentenbemessung und des Alterskapitals kein Anspruch auf eine bestimmte Leistung.`,
    },
    synthesis: {
      title: "Meine Daten",
      warning: `e-renten.ch ist ein Dienst, der es ermöglicht, online eine Brechnung von Renten und Alterskapital vorzunehmen. Das Ergebnis wird mithilfe vereinfachter Verfahren ermittelt und stellt keine Garantie dar. Da die Berechnung hypothetische Elemente enthält, handelt es sich bei den angegebenen Beträgen um Richtwerte. Diese Beträge haben daher keinen rechtlichen Wert und verpflichten in keiner Weise die verschiedenen Organisationen, mit denen sich e-rentes.ch zur Erstellung der Berechnungen austauscht. Darüber hinaus ergibt sich aus der Rentenbemessung und des Alterskapitals kein Anspruch auf eine bestimmte Leistung.`,
      actions: {
        modify_personal_data: "Meine staatliche Altersrente erneut berechnen",
        simulate_data_changes: "Simulationen von Statusänderungen durchführen",
      },
      without_change: "Ohne<br/> Änderung",
      with_change: "Mit<br/> Änderung",
      parts: {
        total: {
          title: "Voraussichtliche Renten & Alterskapital in CHF",
          annuity_label: "Gesamtbetrag der monatlichen Renten",
          capital_label: "Summe des Kapital",
        },
        lavs: {
          title:
            "Staatliche Altersrente <small>(LAVS – 1. Säule)</small> in CHF",
          from: "von",
          to: "bis",
          annuity_avs_label: "Monatliche AVH-Rente",
          partner_annuity_from:
            "Ihr Ehepartner bezieht eine staatliche Altersrente ab dem",
        },
        lpp: {
          title:
            "Berufliche Altersrente <small>(LPP – 2. Säule)</small> in CHF",
          action_label: "Zur Berechnung Ihrer Rente",
          action: "Loggen Sie sich bei Ihrer beruflichen Vorsorgekasse ein",
          annuity_label: "Monatliche Rente",
        },
        "3a": {
          title: "Private Vorsorge  <small>(3. Säule - 3a)</small> in CHF",
          action_label: "Zur Berechnung Ihrer privaten Vorsorge",
          action: "Ihre Informationen hinzufügen",
          capital_label: "Wert Ihrer 3a zum aktuellen Datum (ohne Projektion)",
          capital_until: "Monatliche Zahlung bis zum",
          capital_projection_label: "Prognostizierte Alterskapital",
          capital_projection_from: "ab",
          capital_projection_to: "am",
        },
      },
      modals: {
        simulate: {
          intro:
            "Jede Änderung der eingegebenen Informationen führt zu einer Anpassung der bereitgestellten Simulationen.",
          title: "Änderungen der Daten",
          form: {
            current_salary: "Aktueller Bruttogehalt",
            future_salary: "Gewünschtes Bruttogehalt",
            current_rate: "Aktuelle Aktivitätsrate",
            future_rate: "Gewünschte Aktivitätsrate",
            date: "Renteneintrittsdatum",
            action: "Die Simulation starten",
            outro: `Bei der AHV besteht die Möglichkeit, den Ruhestand ein bis zwei Jahre vor Erreichen des gesetzlichen Mindestalters vorzuziehen oder ihn im Gegenteil um einen Zeitraum von bis zu fünf Jahren aufzuschieben.<br/><br/>

    Die gleiche Flexibilität gilt für die 2. Säule, jedoch hängt ihr Umfang von den Bestimmungen Ihrer Pensionskasse ab.`,
          },
        },
        total: {
          title: "Staatliche Altersrente: Genaue Angaben",
          content: `Die erste Teil der Zusammenfassung ist eine Zusammenfassung der folgenden drei Abschnitte und gibt die Berechnung an:
<br/>
- der monatlichen Rente der 1. Säule zusammen mit der monatlichen Rente der 2. Säule<br/>
- und des Kapitals der privaten Vorsorge (3a).`,
        },
        avs: {
          title: "Die erforderlichen Informationen zur AHV",
          content: `Der angegebene Betrag entspricht einer Schätzung der monatlichen Rente, die Sie ab dem 65. Lebensjahr aus der 1. Säule erhalten werden. Beachten Sie jedoch, dass sich dieser Betrag ändern wird, sobald auch Ihr Ehepartner das Rentenalter erreicht hat, wenn Sie verheiratet sind.`,
        },
        lpp: {
          title: "Betriebliche Altersrente: Genaue Angaben",
          content: `Der angegebene Betrag entspricht einer Berechnung der monatlichen Rente, die Sie ab dem 65. Lebensjahr aus der 2. Säule erhalten werden.`,
        },
        "3a": {
          title: "Rente aus der privaten Vorsorge : Genaue Angaben",
          content: `e-rentes.ch kann zwei Arten der dritten Säule verarbeiten:
<br/>
- 3a (gebundene Vorsorgekonto mit Anlage)<br/>
- 3a (gebundene Vorsorgekonto ohne Anlage)`,
        },
      },
    },
  },
};
