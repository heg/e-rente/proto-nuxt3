import moment from "moment";

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.provide("moment", moment);
});
// eventBus.install = function (Vue) {
//   Vue.prototype.$eventBus = new Vue()
// }

// Vue.use(eventBus)

/*
// Event emit
this.$eventBus.$emit('test-event')
or
app.$eventBus.$emit('test-event')

// Event Listen
this.$eventBus.$on('test-event', () => {})
 */
