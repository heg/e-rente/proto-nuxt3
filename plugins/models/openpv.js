export default (nuxtApp) => () => ({
  getRegistry() {
    return new Promise((resolve, reject) => {
      if (!nuxtApp.$store.state.openpv.registry) {
        const Api = nuxtApp.$api;
        return Api.get("/openpv/entities")
          .then((data) => {
            nuxtApp.$store.commit("openpv/setRegistry", data);
            resolve(data);
          })
          .catch(() => {
            reject(new Error("no registry"));
          });
      } else {
        resolve(nuxtApp.$store.state.openpv.registry);
      }
    });
  },
  getSelection() {
    return new Promise((resolve, reject) => {
      if (!nuxtApp.$store.state.openpv.selection) {
        const Api = nuxtApp.$api;
        return Api.get("/openpv/user/entities")
          .then((data) => {
            nuxtApp.$store.commit("openpv/setSelection", data);
            resolve(data);
          })
          .catch(() => {
            reject(new Error("no selection"));
          });
      } else {
        resolve(nuxtApp.$store.state.openpv.selection);
      }
    });
  },
  get() {
    return new Promise((resolve, reject) => {
      if (!nuxtApp.$store.state.openpv.info) {
        const Api = nuxtApp.$api;
        return Api.get("/open3k/info")
          .then((data) => {
            nuxtApp.$store.commit("openpv/setInfo", data);
            resolve(data);
          })
          .catch(() => {
            reject(new Error("no info"));
          });
      } else {
        resolve(nuxtApp.$store.state.openpv.info);
      }
    });
  },
  getProvident() {
    return new Promise((resolve, reject) => {
      if (!nuxtApp.$store.state.openpv.providents) {
        const Api = nuxtApp.$api;
        return Api.get("/openpv/user/providents")
          .then((data) => {
            nuxtApp.$store.commit("openpv/setProvidents", data);
            this.save(Object.values(data).pop()).then(() => {
              resolve(data);
            });
          })
          .catch(() => {
            reject(new Error("no provident"));
          });
      } else {
        resolve(nuxtApp.$store.state.openpv.providents);
      }
    });
  },
  save(info) {
    nuxtApp.$store.commit("openpv/setInfo", info);
    const Api = nuxtApp.$api;
    return Api.post("/open3k/info", info);
  },
  reset() {
    nuxtApp.$store.commit("openpv/setInfo", null);
    return new Promise((resolve, reject) => {
      this.get()
        .then((info) => {
          this.save({
            solde: null,
            rate: null,
            amount: null,
            paydate: info.paydate,
          }).finally(() => {
            resolve();
          });
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  saveSelection(ids) {
    nuxtApp.$store.commit("openpv/setProvidents", null);
    nuxtApp.$store.commit("openpv/setSelection", null);
    const Api = nuxtApp.$api;
    return Api.post("/openpv/user/entities", {
      ids,
    });
  },
});
