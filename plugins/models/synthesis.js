export default (nuxtApp) => () => ({
  get(simul = null) {
    const Api = nuxtApp.$api;
    return Api.get("/synthesis/digest", simul);
  },
});
