export default (nuxtApp) => () => ({
  getMe(force) {
    return new Promise((resolve, reject) => {
      if (force || nuxtApp.$store.state.user.ttl < Date.now()) {
        const Api = nuxtApp.$api;
        return Api.get("/me")
          .then((data) => {
            nuxtApp.$store.commit("user/setLogin", data);
            nuxtApp.$store.commit("user/setTTL", 60 * 5);
            resolve(data);
          })
          .catch((data) => {
            nuxtApp.$store.commit("user/setTTL", 10);
            reject(data);
          });
      } else if (nuxtApp.$store.state.user.login) {
        resolve(nuxtApp.$store.state.user.login);
      } else {
        reject(new Error("not logged"));
      }
    });
  },
});
