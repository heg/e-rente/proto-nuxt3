export default (nuxtApp) => () => ({
  saveInfo(form) {
    nuxtApp.$store.commit("openak/setInfo", null);
    nuxtApp.$store.commit("openak/setPension", null);
    const Api = nuxtApp.$api;
    return Api.post("/openak/pension-calculator/info", form);
  },
  getInfo() {
    return new Promise((resolve, reject) => {
      if (!nuxtApp.$store.state.openak.info) {
        const Api = nuxtApp.$api;
        return Api.get("/openak/pension-calculator/info")
          .then((data) => {
            nuxtApp.$store.commit("openak/setInfo", data);
            resolve(data);
          })
          .catch(() => {
            reject(new Error("no info"));
          });
      } else {
        resolve(Object.assign({}, nuxtApp.$store.state.openak.info));
      }
    });
  },
  getInsuredInfo(navs) {
    return new Promise((resolve, reject) => {
      if (!nuxtApp.$store.state.openak.insureds[navs]) {
        const Api = nuxtApp.$api;
        return Api.get(`/openak/insureds/${navs}`)
          .then((insured) => {
            nuxtApp.$store.commit("openak/saveInsured", {
              navs,
              insured,
            });
            resolve(insured);
          })
          .catch((err) => {
            reject(err);
          });
      } else {
        resolve(Object.assign({}, nuxtApp.$store.state.openak.insureds[navs]));
      }
    });
  },
  get(force = false) {
    return new Promise((resolve, reject) => {
      if (force || !nuxtApp.$store.state.openak.pension) {
        const Api = nuxtApp.$api;
        return Api.get("/openak/pension-calculator")
          .then((data) => {
            nuxtApp.$store.commit("openak/setPension", data);
            resolve(data);
          })
          .catch((err) => {
            reject(err);
          });
      } else {
        resolve(Object.assign({}, nuxtApp.$store.state.openak.pension));
      }
    });
  },
});
