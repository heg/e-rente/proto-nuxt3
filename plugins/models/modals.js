export default (nuxtApp) => () => ({
  _emit(event) {
    const bus = nuxtApp.$eventBus;
    return bus.$emit(event);
  },
  _on(event, callback, action = "on") {
    const bus = nuxtApp.$eventBus;
    return bus["$" + action](event, callback);
  },
  open(id = "modal") {
    return this._emit("open-modal-" + id);
  },
  onOpen(id = "modal", callback) {
    return this._on("open-modal-" + id, callback);
  },
  emitIsOpen() {
    return this._emit("modal-is-open");
  },
  emitIsClosed() {
    return this._emit("modal-is-closed");
  },
  onIsOpen(callback, action = "on") {
    return this._on("modal-is-open", callback, action);
  },
  onIsClosed(callback, action = "on") {
    return this._on("modal-is-closed", callback, action);
  },
});
