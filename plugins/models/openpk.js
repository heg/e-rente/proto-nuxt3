const clone = function (object) {
  return JSON.parse(JSON.stringify(object));
};

export default (nuxtApp) => () => ({
  reset() {
    return new Promise((resolve, reject) => {
      this.saveSelection([])
        .then(() => {
          return resolve();
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  deleteFund(fundId) {
    return new Promise((resolve, reject) => {
      this.getSelection()
        .then((selection) => {
          const tempSelection = clone(selection);
          const index = tempSelection.indexOf(fundId);
          if (index < 0) {
            return resolve();
          }
          tempSelection.splice(index, 1);
          this.saveSelection(tempSelection).then(() => {
            return resolve();
          });
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  getRegistry() {
    return new Promise((resolve, reject) => {
      if (!nuxtApp.$store.state.openpk.registry) {
        const Api = nuxtApp.$api;
        return Api.get("/openpk/pension-funds")
          .then((data) => {
            nuxtApp.$store.commit("openpk/setRegistry", data);
            resolve(data);
          })
          .catch(() => {
            reject(new Error("no registry"));
          });
      } else {
        resolve(nuxtApp.$store.state.openpk.registry);
      }
    });
  },
  getLoginRegistry() {
    return new Promise((resolve, reject) => {
      if (!nuxtApp.$store.state.openpk.loginRegistry) {
        const Api = nuxtApp.$api;
        return Api.get("/login/pension-funds")
          .then((data) => {
            nuxtApp.$store.commit("openpk/setLoginRegistry", data);
            resolve(data);
          })
          .catch(() => {
            reject(new Error("no registry"));
          });
      } else {
        resolve(nuxtApp.$store.state.openpk.loginRegistry);
      }
    });
  },
  getPolicy() {
    return new Promise((resolve, reject) => {
      if (!nuxtApp.$store.state.openpk.policies) {
        const Api = nuxtApp.$api;
        return Api.get("/openpk/user/pension-funds/policies")
          .then((data) => {
            nuxtApp.$store.commit("openpk/setPolicies", data);
            resolve(data);
          })
          .catch(() => {
            reject(new Error("no policy"));
          });
      } else {
        resolve(nuxtApp.$store.state.openpk.policies);
      }
    });
  },
  getSelection() {
    return new Promise((resolve, reject) => {
      if (!nuxtApp.$store.state.openpk.selection) {
        const Api = nuxtApp.$api;
        return Api.get("/openpk/user/pension-funds")
          .then((data) => {
            nuxtApp.$store.commit("openpk/setSelection", data);
            resolve(data);
          })
          .catch(() => {
            reject(new Error("no selection"));
          });
      } else {
        resolve(nuxtApp.$store.state.openpk.selection);
      }
    });
  },
  saveSelection(ids) {
    nuxtApp.$store.commit("openpk/setPolicies", null);
    nuxtApp.$store.commit("openpk/setSelection", null);
    const Api = nuxtApp.$api;
    return Api.post("/openpk/user/pension-funds", {
      ids,
    });
  },
});
