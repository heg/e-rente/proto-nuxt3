import User from "./models/user.js";
import Modals from "./models/modals.js";
import OpenAK from "./models/openak.js";
import OpenPK from "./models/openpk.js";
import OpenPV from "./models/openpv.js";
import Loader from "./models/loader.js";
import Synthesis from "./models/synthesis.js";

export default defineNuxtPlugin((nuxtApp) => {
  // console.log(nuxtApp);
  // console.log(nuxtApp);
  // console.log(nuxtApp);
  // const context = nuxtApp.nuxt2Context;
  const models = {
    User: User(nuxtApp),
    Modals: Modals(nuxtApp),
    OpenAK: OpenAK(nuxtApp),
    OpenPK: OpenPK(nuxtApp),
    OpenPV: OpenPV(nuxtApp),
    Loader: Loader(nuxtApp),
    Synthesis: Synthesis(nuxtApp),
  };

  nuxtApp.provide("models", models);
});
