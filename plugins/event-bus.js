import Emitter from "tiny-emitter";

export default defineNuxtPlugin((nuxtApp) => {
  const emitter = new Emitter();
  emitter.$off = emitter.off;
  emitter.$emit = emitter.emit;
  emitter.$once = emitter.once;
  emitter.$on = emitter.on;
  nuxtApp.provide("eventBus", emitter);
});
// eventBus.install = function (Vue) {
//   Vue.prototype.$eventBus = new Vue()
// }

// Vue.use(eventBus)

/*
// Event emit
this.$eventBus.$emit('test-event')
or
app.$eventBus.$emit('test-event')

// Event Listen
this.$eventBus.$on('test-event', () => {})
 */
