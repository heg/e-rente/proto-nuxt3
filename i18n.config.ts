export default defineI18nConfig(() => ({
  locales: [
    { code: "fr", label: "FR", iso: "fr-CH", file: "fr.js" },
    { code: "de", label: "DE", iso: "de-CH", file: "de.js", dir: "ltr" },
  ],
  defaultLocale: "fr",
  langDir: "~/locales/",
  fallbackLocale: "fr",

  vueI18n: {
    fallbackLocale: "fr",
  },
}));
