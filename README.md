# Prototype du Client pour le projet DIBS/e-rentes.ch

Ce client front interroge [l'api du projet](https://framagit.org/heg/e-rente/proto-api) qui fait office de back du projet.

Le déploiement de production du client peut être trouvé ici : [https://e-rentes.ch](https://e-rentes.ch)

## Documentation

La documentation du projet [se trouve ici](docs/index.md).

## Prérequis

- **nvm** Pour l'installation du projet en local nous vous conseillons d'utiliser [nvm](https://github.com/nvm-sh/nvm)

## Installation du projet

```
nvm use
nvm install
npm install
```

## Mise en place des variables d'environnement

```
cp .env.example .env
```

puis modifier le nouveau fichier `.env`

Voici les différentes variables à remplir :

- `HOST` : l'hote à laquelle vous voulez lancer le serveur : si local, `0.0.0.0`
- `PORT`: le port sur lequel vous voulez lancer le serveur (`4001` par exemple)
- `API_BASE_PATH`: l'url à laquelle vous faite tourner [l'api du projet](https://framagit.org/heg/e-rente/proto-api)

## Lancer le serveur

- `npm run dev`
