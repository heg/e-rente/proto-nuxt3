export default defineNuxtRouteMiddleware(async () => {
  const nuxtApp = useNuxtApp();
  try {
    await nuxtApp.$models.User().getMe();
    return navigateTo(nuxtApp.$localePath("/router"));
  } catch (e) {}
});
