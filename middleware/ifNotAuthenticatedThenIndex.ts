export default defineNuxtRouteMiddleware(async () => {
  const nuxtApp = useNuxtApp();
  try {
    await nuxtApp.$models.User().getMe();
  } catch (e) {
    return navigateTo(nuxtApp.$localePath("/"));
  }
});
